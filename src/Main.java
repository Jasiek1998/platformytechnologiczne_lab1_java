import java.io.File;

public class Main {
    public static void main(String[] args) {
        MyComparator toCompare = null;
        String path = args[0];
        boolean recursion = false;

        for (String arg : args) {
            if (arg.equals("--lengthSorted"))
                toCompare = new MyComparator();
            if (arg.equals("--recursive"))
                recursion = true;
        }


        File file = new File(path);
        if (!file.exists()) {
            System.out.println("File doesn't exist");
        } else {
            DiskElement element = new DiskElement(file, toCompare, recursion);
            element.print(0);
        }
    }
}
