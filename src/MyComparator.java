import java.util.Comparator;

public class MyComparator implements Comparator {


    @Override
    public int compare(Object object1, Object object2) {
        DiskElement firstElement = (DiskElement) object1;
        DiskElement secondElement = (DiskElement) object2;
        if (firstElement.getFile().getName().length() > secondElement.getFile().getName().length())
            return 1;
        else if (firstElement.getFile().getName().length() < secondElement.getFile().getName().length())
            return -1;
        else return firstElement.compareTo(secondElement);
    }
}
