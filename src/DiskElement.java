import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Set;
import java.util.TreeSet;

public class DiskElement implements Comparable<DiskElement> {
    Set<DiskElement> childFiles;
    private File file;
    private boolean isFile;
    private boolean isRecursive;

    public File getFile() {
        return file;
    }

    public DiskElement(File file, MyComparator toCompare, boolean recursion) {
        this.file = file;
        this.isFile = !(file.isDirectory());
        this.isRecursive=recursion;

        if (!this.isFile) {
            if (toCompare == null)
                this.childFiles = new TreeSet<>();
            else
                this.childFiles = new TreeSet<>(toCompare);

            File[] files = file.listFiles();
            for (File f : files)
                this.childFiles.add(new DiskElement(f, toCompare, recursion));
        }

    }


    public void print(int depth) {
        SimpleDateFormat template = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");

        if (depth > 0) {
            for (int i = 0; i < depth; i++)
                System.out.print("-");
        }

        if (this.isFile) {
            System.out.print(file.getName());
            for(int j=0;j<70-depth-this.getFile().getName().length();j++)
                System.out.print(" ");
            System.out.println("P \t" + this.getSize() + " bytes \t \t" + (template.format(file.lastModified())));
        }

        else {
            System.out.print(file.getName());
            for(int j=0;j<70-depth-this.getFile().getName().length();j++)
                System.out.print(" ");
            System.out.println("P \t" + this.getSize() + " files \t \t" + (template.format(file.lastModified())));
        }

        if(depth>0 && !isRecursive)
            ;
        else {
            if (!this.isFile) {
                for (DiskElement element : childFiles)
                    element.print(depth + 1);
            }
        }
    }



    public long getSize() {
        if (file.isFile())
            return file.length();
        else
            return file.listFiles().length;
    }


    @Override
    public int compareTo(DiskElement element) {
        return (this.file.getName().toLowerCase().compareTo(element.file.getName().toLowerCase()));
    }


    public boolean equals(DiskElement element) {
        return this.file.equals(element.file);
    }


    public boolean equals(Object obj) {
        if (obj == null) return false;
        else if (getClass() != obj.getClass()) return false;
        else return (this.compareTo((DiskElement) obj) == 0);
    }


    public int hashCode() {
        return ((int) this.getSize() * 4 + this.file.hashCode());
    }
}
